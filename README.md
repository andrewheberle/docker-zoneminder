# docker-zoneminder

This is a container to run Zoneminder as a container in much the same way as the official container with the following differences:

1. A database is NOT included, so you must run a seperate database conatiner (mysql or mariadb)
1. The ZM Events Server is not included at this time
1. No SSL support. You must add a reverse proxy to perform SSL termination
1. Some basic support for multi-server deployments under Kubernetes
1. The web server (nginx) add some additional headers (CORS mainly)

# Images

There are two images as follows:

| Image                                                            | Info                    |
|------------------------------------------------------------------|-------------------------|
| registry.gitlab.com/andrewheberle/zoneminder                     | Zoneminder (base image) |
| registry.gitlab.com/andrewheberle/zoneminder/db-init             | DB creation             |

All images are tagged consistently so are based on the same version of Zoneminder.

# Image Tags

Tags are based on the Zoneminder release using the semver scheme here: https://github.com/bep/semverpair

An example tag with relevant Zoneminder version number are in bold is: **1.36**00.**11**00

Any local patches, image changes or fixes will be made clear via incrementing the minor or patch version.

In general the `latest` tag should be avoided as this may be broken or incomplete at any point in time.
# Usage

The examples below will result in a single zoneminder instance being accessible at:

`http://<hostname>:8080/zm/`

You should (**really really**) customise a number of things below before going into production:

1. Timezones (via the `TZ` environment variable)
1. Use a specific image tag for Zoneminder (tags are based on Zoneminder stable release versions)
1. Choose a specific mysql/mariadb tag rather than latest
1. Set a strong password for the Zoneminder DB (via the `ZM_DB_PASS` environment variable)
1. Deploy a reverse proxy in front of the `zoneminder` container to perform SSL termination (this can be another container)
1. Set the `shm` to half your RAM size for the docker and docker-compose options (this is the default for Kubernetes at this stage)

## Docker

The commands below perform the following steps:

1. Create some locations for persistent storage
1. Create a Docker network to connect the database and zoneminder containers
1. Dump the require SQL scripts to initialise a new database (this only has any effect on the first start of the database container)
1. Starts the database and zoneminder containers

```sh
mkdir -p /path/to/db/init /path/to/db/data /path/to/zm/data
docker network create zoneminder
docker run --rm registry.gitlab.com/andrewheberle/docker-zoneminder/db-init:latest > /path/to/db/init/init.sql
docker run -d \
    --name database \
    --network zoneminder \
    --restart unless-stopped \
    -e TZ="Etc/UTC" \
    -e MYSQL_RANDOM_ROOT_PASSWORD="yes" \
    -v /path/to/db/init:/docker-entrypoint-initdb.d \
    -v /path/to/db/data:/var/lib/mysql \
    mariadb:latest
docker run -d \
    --name zoneminder \
    --network zoneminder \
    --restart unless-stopped \
    --shm-size 8G \
    -e TZ="Etc/UTC" \
    -p 8080:8080 \
    -v /path/to/zm/data:/data \
    registry.gitlab.com/andrewheberle/docker-zoneminder:latest
```

## Docker Compose

The following compose file and the subsequent commands will bring up both containers:

```yaml
version: '3.1'
services:
  zoneminder:
    container_name: zoneminder
    image: registry.gitlab.com/andrewheberle/docker-zoneminder:latest
    restart: unless-stopped
    ports:
    - 8080:8080/tcp
    shm_size: 8G
    environment:
    - TZ=Etc/UTC
    volumes:
    - /path/to/zm/data:/data:rw

  database:
    container_name: database
    image: mariadb:latest
    restart: unless-stopped
    environment:
    - TZ=Etc/UTC
    - MYSQL_RANDOM_ROOT_PASSWORD=yes
    volumes:
    - /path/to/db/init:/docker-entrypoint-initdb.d:ro
    - /path/to/db/data:/var/lib/mysql:rw
```

Bring things up (assuming you have your compose file in the current directory):

```sh
mkdir -p /path/to/db/init /path/to/db/data /path/to/zm/data
docker run --rm registry.gitlab.com/andrewheberle/docker-zoneminder/db-init:latest > /path/to/db/init/init.sql
docker-compose up -d
```

## Kubernetes

Example manifests are in the `manifests/` directory in this repo.

There are a number of assumptions made by these manifests:

1. You have a working HAProxy based ingress controller
1. A default storage class exists for provisioning volume claims
1. You have a namespace named `zoneminder` already created

# Configuration

Configuration is via environment variables only.

| Variable             | Default       | Use                                                         |
|----------------------|---------------|-------------------------------------------------------------|
| ZM_FCGIWRAP_CHILDREN | 4             | Sets the numer of FCGI processes                            |
| ZM_PHPFPM_MAX        | 5             | Maximum number of PHP-FPM processes                         |
| ZM_PHPFPM_START      | 2             | Initial number of PHP-FPM processes                         |
| ZM_PHPFPM_SPARE_MIN  | 1             | Minimum spare PHP-FPM processes                             |
| ZM_PHPFPM_SPARE_MAX  | 3             | Maximum spare PHP-FPM processes                             |
| ZM_DB_HOST           | database      | Hostname of the DB container                                |
| ZM_DB_USER           | zmuser        | Database username                                           |
| ZM_DB_PASS           | zmpass        | Database password                                           |
| ZM_DB_NAME           | zm            | Database name                                               |
| ZM_DIR_EVENTS        | /data/events  | Location of Events dir                                      |
| ZM_DIR_EXPORTS       | /data/exports | Location of Exports dir                                     |
| ZM_PATH_LOGS         | /data/logs    | Location of Logs                                            |
| ZM_PATH_SWAP         | /data/swap    | Swap location                                               |
| ZM_MULTI_SERVER      | no            | Enable multi-server (sets ZM_SERVER_HOST based on hostname) |
| ZM_SERVER_HOST       | Not set       | Sets ZM_SERVER_HOST manually for multi-server               |

# Multi-Server

The process to scale up additional Zoneminder servers under Kubernetes is:

1. Set the `ZM_MULTI_SERVER` environment variable to `yes` in the `ConfigMap` resource
1. Increase the replica count in the `StatefulSet` resource
1. Add a new `Service` resource for each replica:
    ```yaml
    apiVersion: v1
    kind: Service
    metadata:
      name: zm-server-X
      namespace: zoneminder
    spec:
      ports:
      - name: http
        port: 8080
        protocol: TCP
        targetPort: 8080
      selector:
        app: zoneminder
        service: zoneminder
        statefulset.kubernetes.io/pod-name: zoneminder-X
      type: ClusterIP
    ```
1. Add a new `Ingress` for each `Service`:
    ```yaml
    apiVersion: networking.k8s.io/v1
    kind: Ingress
    metadata:
      name: zm-server-X
      namespace: zoneminder
    spec:
      rules:
      - host: zm-X.example.net
        http:
          paths:
          - backend:
              service:
                name: zm-server-X
                port:
                  number: 8080
            path:
            pathType: ImplementationSpecific
    ```
1. Add entries to your Zoneminder config for each instance using the correct URLs (as per the `Ingress` rules)
1. Add the required DNS records so you can access each instance correctly

## Multi-Server Issues

When accessing ZoneMinder via the "main" URL (http://zm.example.net:8080 in these examples) a warning is logged saying that this URL is not found in the servers list.

Based on my testing this warning does not impact functionality at all.
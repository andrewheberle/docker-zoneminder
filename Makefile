TAG ?= latest
CACHE_TAG ?= latest

ifndef IMAGE
$(error IMAGE must be provided)
endif

all: base db-init

base:
	make -C $@/ IMAGE=$(IMAGE) CACHE_TAG=$(CACHE_TAG) build
	docker tag "$(IMAGE)" "$(IMAGE):$(TAG)"
	docker push "$(IMAGE):$(TAG)"
	docker push "$(IMAGE)"

db-init:
	make -C $@/ IMAGE=$(IMAGE) TAG=$(TAG) build
	docker tag "$(IMAGE)/$@" "$(IMAGE)/$@:$(TAG)"
	docker push "$(IMAGE)/$@:$(TAG)"
	docker push "$(IMAGE)/$@"

.PHONY: all base db-init
#!/bin/sh
sed 's/^source .*$//' /usr/share/zoneminder/db/zm_create.sql
cat /usr/share/zoneminder/db/triggers.sql
printf '\nGRANT lock tables,alter,create,index,select,insert,update,delete,drop,trigger,create routine,alter routine,execute ON zm.* TO '\''%s'\'' IDENTIFIED BY "%s";\n' "$ZM_DB_USER" "$ZM_DB_PASS"
printf 'GRANT super ON *.* TO '\''%s'\'' IDENTIFIED BY "%s";\n' "$ZM_DB_USER" "$ZM_DB_PASS"
